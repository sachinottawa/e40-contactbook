// 1. event listener for form submission
const form = document.getElementById('form')
const ul = document.getElementById('contactsList')

form.addEventListener('submit', handleFormSubmission)

function handleFormSubmission(event) {
    // prevent the default page refresh
    event.preventDefault()
    // reading name, phone and image from form
    const name = form['name'].value
    const phone = form['phone'].value
    const image = form['image'].value

    if (!name) {
        console.log("name cannot be null")
        return
    }
    else if (name.trim() === "") {
        console.log("name cannot be just spaces")
        return
    }
    // creating a contact using name, phone and image
    const contact = createContact(name, phone, image)
    // adding the contact to screen
    ul.appendChild(contact)
}
// 2. create a contact
function createContact(name, phone, image) {

    const li = document.createElement('li')

    const img = document.createElement('img')
    img.src = image
    li.appendChild(img)

    const div = document.createElement('div')
    li.appendChild(div)

    const h3 = document.createElement('h3')
    h3.innerHTML = name
    div.appendChild(h3)

    const span = document.createElement('span')
    span.innerHTML = phone
    div.appendChild(span)

    return li
}

// 3. add contact to HTML
